<?php

namespace Database\Seeders;

use App\Models\Location;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('locations')->truncate();

        $data = [
            [
                'code' => '01',
                'name' => 'KC Bandara Soekarno Hatta',
            ],
            [
                'code' => '02',
                'name' => 'KC Bandara Halim Perdanakusuma',
            ],
            [
                'code' => '03',
                'name' => 'KC Bandara Blimbingsar (Banyuwangi)',
            ],
            [
                'code' => '04',
                'name' => 'KC Bandara Silangit',
            ],
            [
                'code' => '05',
                'name' => 'KC Bandara Sultan Thaha',
            ],
            [
                'code' => '06',
                'name' => 'KC Bandara Raja Haji Fisabilillah',
            ],
            [
                'code' => '07',
                'name' => 'KC Bandara Husein Satstranegara',
            ],
            [
                'code' => '08',
                'name' => 'KC Bandara Minangkabau',
            ],
            [
                'code' => '09',
                'name' => 'KC Bandara Depati Amir',
            ],
            [
                'code' => '10',
                'name' => 'KC Bandara Sultan Iskandar Muda',
            ],
            [
                'code' => '11',
                'name' => 'KC Bandara Sultan Syarif Kasim II',
            ],
            [
                'code' => '12',
                'name' => 'KC Bandara Supadio',
            ],
            [
                'code' => '13',
                'name' => 'KC Bandara Sultan Mahmud Badarudin II',
            ],
            [
                'code' => '14',
                'name' => 'KANTOR PUSAT PT APS',
            ],
        ];

        Location::insert($data);
    }
}
