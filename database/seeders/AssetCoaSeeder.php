<?php

namespace Database\Seeders;

use App\Models\AssetCoa;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AssetCoaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('asset_coas')->truncate();

        $data = [
            [
                'code' => '1221101',
                'name' => 'Partition - Office',
            ],
            [
                'code' => '1221201',
                'name' => 'Furniture and Fixture - Office',
            ],
            [
                'code' => '1221301',
                'name' => 'Equipment - Office',
            ],
            [
                'code' => '1221401',
                'name' => 'Computer - Office',
            ],
            [
                'code' => '1221501',
                'name' => 'Vehicle 4 Wheels - Office',
            ],
            [
                'code' => '1221601',
                'name' => 'Property and Equipment - Security & Avsec',
            ],
            [
                'code' => '1221701',
                'name' => 'Property and Equipment - Regulated Agent',
            ],
            [
                'code' => '1222101',
                'name' => 'Property and Equipment - Saphire',
            ],
            [
                'code' => '1222103',
                'name' => 'Property and Equipment - Lounge',
            ],
            [
                'code' => '1222301',
                'name' => 'Property and Equipment - Media',
            ],
            [
                'code' => '1224101',
                'name' => 'Property and Equipment - IT Support',
            ],
            [
                'code' => '1225101',
                'name' => 'Property and Equipment - ICT',
            ],
            [
                'code' => '1226101',
                'name' => 'Property and Equipment - Parking',
            ],
            [
                'code' => '1229101',
                'name' => 'Property and Equipment - Cleaning Services',
            ],
            [
                'code' => '1241003',
                'name' => 'Property and Equipment - Software',
            ],
        ];

        AssetCoa::insert($data);
    }
}
