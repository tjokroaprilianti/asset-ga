<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            AssetCategorySeeder::class,
            AssetCoaSeeder::class,
            AssetStatusSeeder::class,
            LocationSeeder::class,
            UserGroupSeeder::class,
            VendorSeeder::class,
            UnitSeeder::class,
            ProvinsiSeeder::class
        ]);
    }
}
