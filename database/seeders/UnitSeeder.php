<?php

namespace Database\Seeders;

use App\Http\Controllers\UnitController;
use App\Models\Unit;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('units')->truncate();

        $data = [
            ['name' => 'FINANCE', 'code' => '-', 'cost_center_code' => '-'],
            ['name' => 'GA & PROCUREMENT', 'code' => '-', 'cost_center_code' => '-'],
            ['name' => 'HUMAN RESOURCE', 'code' => '-', 'cost_center_code' => '-'],
            ['name' => 'APS AVIATION SECURITY', 'code' => '-', 'cost_center_code' => '-'],
            ['code' => '-', 'cost_center_code' => '-', 'name' => 'APS DIGITAL', 'children' => [
                ['name' => 'ICT', 'code' => '-', 'cost_center_code' => '-'],
                ['name' => 'SUPPORT', 'code' => '-', 'cost_center_code' => '-'],
            ]],
            ['code' => '-', 'cost_center_code' => '-', 'name' => 'FACILITY SERVICES', 'children' => [
                ['name' => 'PARKIR', 'code' => '-', 'cost_center_code' => '-'],
                ['name' => 'OPERATION AND MAINTENANCE', 'code' => '-', 'cost_center_code' => '-'],
            ]],
            ['code' => '-', 'cost_center_code' => '-', 'name' => 'APS PASSENGER SERVICES', 'children' => [
                ['name' => 'SAPHIRE', 'code' => '-', 'cost_center_code' => '-'],
                ['name' => 'LOUNGE', 'code' => '-', 'cost_center_code' => '-'],
                ['name' => 'REGULATED AGENT', 'code' => '-', 'cost_center_code' => '-'],
                ['name' => 'HELPER', 'code' => '-', 'cost_center_code' => '-'],
                ['name' => 'CUSTOMER SERVICES', 'code' => '-', 'cost_center_code' => '-'],
            ]],
            ['code' => '-', 'cost_center_code' => '-', 'name' => 'APS RETAIL & MEDIA', 'children' => [
                ['name' => 'MEDIA', 'code' => '-', 'cost_center_code' => '-'],
                ['name' => 'WRAPPING', 'code' => '-', 'cost_center_code' => '-'],
                ['name' => 'RETAIL', 'code' => '-', 'cost_center_code' => '-'],
            ]]
        ];

        foreach ($data as $d) {
            $unit = Unit::create(['name' => $d['name'], 'code' => $d['code']]);

            if (isset($d['children'])) {
                foreach ($d['children'] as $c) {
                    Unit::create([
                        'code' => $c['code'],
                        'name' => $c['name'],
                        'parent_id' => $unit->id
                    ]);
                }
            }
        }
    }
}
