<?php

namespace Database\Seeders;

use App\Models\UserGroup;
use Illuminate\Database\Seeder;

class UserGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'ADMIN',
                'description' => 'Administrator'
            ],
            [
                'name' => 'OPERATOR',
                'description' => 'Operator'
            ],
        ];

        UserGroup::insert($data);
    }
}
