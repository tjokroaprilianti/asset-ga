<?php

namespace Database\Seeders;

use App\Models\AssetStatus;
use Illuminate\Database\Seeder;

class AssetStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'code' => 'BA',
                'name' => 'Baik',
            ],
            [
                'code' => 'NO',
                'name' => 'Hilang',
            ],
            [
                'code' => 'R1',
                'name' => 'Rusak Bisa Diperbaiki',
            ],
            [
                'code' => 'R2',
                'name' => 'Rusak Tidak Bisa Diperbaiki',
            ],
        ];

        AssetStatus::insert($data);
    }
}
