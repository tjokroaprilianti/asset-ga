<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Provinsi;
use Illuminate\Support\Facades\Storage;


use DB;

class ProvinsiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $path = storage_path('app/public/data/datajadi.json');
        $data = json_decode(file_get_contents('datajadi.json'), true);

        foreach ($data['data']['features'] as $key => $value) {
            // foreach ($value['geometry']['coordinates'] as $x => $y) {
            //     # code...
            //     dd($y);
            // }
            DB::table('provinsis')
                ->insert([
                    'city' => $value['properties']['A1KPKNAME'],
                    'type' => $value['geometry']['type'],
                    'coordinates' => json_encode($value['geometry']['coordinates']),
                    'Shape_Leng' => $value['properties']['Shape_Leng'],
                    'Shape_Area' => $value['properties']['Shape_Area'],
                ]);
        }
    }
}
