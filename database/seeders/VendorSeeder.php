<?php

namespace Database\Seeders;

use App\Models\Vendor;
use Illuminate\Database\Seeder;

class VendorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'Vendor 1'],
            ['name' => 'Vendor 2'],
            ['name' => 'Vendor 3'],
            ['name' => 'Vendor 4'],
        ];

        Vendor::insert($data);
    }
}
