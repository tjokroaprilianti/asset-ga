<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProvinsisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provinsis', function (Blueprint $table) {
            $table->id();
            $table->string('city')->nullable();

            //$table->decimal('lng')->nullable();
            //$table->json('latlngs')->nullable();
            $table->string('type')->nullable();
            $table->json('coordinates')->nullable();
            $table->decimal('Shape_Leng')->nullable();
            $table->decimal('Shape_Area')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provinsis');
    }
}
