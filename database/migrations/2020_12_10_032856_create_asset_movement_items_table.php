<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetMovementItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_movement_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('asset_movement_id');
            $table->unsignedBigInteger('asset_id');
            $table->string('trademark')->nullable();
            $table->string('serial_number')->nullable();
            $table->string('model')->nullable();
            $table->smallInteger('qty');
            $table->unsignedBigInteger('status_id');
            $table->unsignedBigInteger('location_id');
            $table->text('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_movement_items');
    }
}
