<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->id();
            $table->string('registration_number');
            $table->string('name');
            $table->string('trademark')->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('model')->nullable();
            $table->string('color')->nullable();
            $table->string('serial_number')->nullable();
            $table->string('part_number')->nullable();
            $table->tinyInteger('lifetime')->nullable();
            $table->bigInteger('price')->nullable();
            $table->bigInteger('value')->nullable();
            $table->smallInteger('year')->nullable();
            $table->date('uselife')->nullable();
            $table->string('type')->nullable();
            $table->unsignedBigInteger('asset_category_id');
            $table->unsignedBigInteger('vendor_id')->nullable();
            $table->unsignedBigInteger('location_id')->nullable();
            $table->unsignedBigInteger('status_id')->nullable();
            $table->unsignedBigInteger('unit_id')->nullable();
            $table->string('picture')->nullable();
            $table->text('remark')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
