<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeSchemaAssetMovement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset_movements', function (Blueprint $table) {
            $table->dropColumn([
                'asset_id',
                'old_location_id',
                'new_location_id',
                'old_status_id',
                'new_status_id',
                'old_unit_id',
                'new_unit_id',
            ]);

            $table->string('number')->nullable();
            $table->json('pihak1')->nullable();
            $table->json('pihak2')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset_movements', function (Blueprint $table) {
            $table->unsignedBigInteger('asset_id')->nullable();
            $table->unsignedBigInteger('old_location_id')->nullable();
            $table->unsignedBigInteger('new_location_id')->nullable();
            $table->unsignedBigInteger('old_status_id')->nullable();
            $table->unsignedBigInteger('new_status_id')->nullable();
            $table->unsignedBigInteger('old_unit_id')->nullable();
            $table->unsignedBigInteger('new_unit_id')->nullable();

            $table->dropColumn(['number', 'pihak1', 'pihak2']);
        });
    }
}
