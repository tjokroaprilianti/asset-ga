<?php

namespace Database\Factories;

use App\Models\AssetStatus;
use Illuminate\Database\Eloquent\Factories\Factory;

class AssetStatusFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AssetStatus::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
