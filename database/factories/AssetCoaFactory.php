<?php

namespace Database\Factories;

use App\Models\AssetCoa;
use Illuminate\Database\Eloquent\Factories\Factory;

class AssetCoaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AssetCoa::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
