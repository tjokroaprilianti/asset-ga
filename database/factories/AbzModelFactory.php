<?php

namespace Database\Factories;

use App\Models\AbzModel;
use Illuminate\Database\Eloquent\Factories\Factory;

class AbzModelFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AbzModel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
