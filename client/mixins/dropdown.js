export default {
  data() {
    return {
      // REGISTER ALL LIST HERE
      userList: [],
      locationList: [],
      userGroupList: [],
      assetCategoryList: [],
      assetStatusList: [],
      assetList: [],
      vendorList: [],
      unitList: [],
      provinsiList: []
    }
  },

  computed: {
    unitFilter() {
      return this.unitList.map(u => {
        return {text: u.name, value: u.id}
      })
    },

    locationFilter() {
      return this.locationList.map(u => {
        return {text: u.name, value: u.id}
      })
    },
    provinsiFilter() {
      return this.provinsiList.map(u => {
        console.log(u)
        return {text: u.city, value: u.id}
      })
    }
  },

  methods: {

    getList(url, keyword, listContainer) {
      this.$axios.get(url, { params: { keyword } }).then(r => {
        this[listContainer] = r.data.data || r.data
      }).catch(e => {
        this.$message({
          message: e.response.data.message,
          type: 'error'
        })
      })
    }

  }
}
