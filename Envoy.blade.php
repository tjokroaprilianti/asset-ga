@servers(['dev' => ['asset-aps.lamsolusi.com']])

@task('deploy', ['on' => 'dev'])
    cd asset-aps
    git pull
    php artisan migrate --force
    cd client && npm run build
@endtask
