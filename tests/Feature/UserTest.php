<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    // public function testIndex()
    // {
    //     $response = $this->get('/api/user');
    //     $response->dump();

    //     $response->assertStatus(200);
    // }

    // public function testShow()
    // {
    //    $response = $this
    //         ->actingAs(User::first())
    //         ->getJson('/api/user/2');
        
    //         $response->dump();
    //     $response->assertStatus(200);
    // }

    public function testStore()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->postJson('/api/user', [
            'name' => 'test name',
            'email' => 'email666@mail.com',
            'password' => 'password'
        ]);
 
        $response->dump();

        $response
            ->assertStatus(201)
            ->assertJson([
                'message' => 'Data has been saved',
            ]);
    }

    // public function testUpdate()
    // {
    //     $response = $this->putJson('/api/user/2', [
    //         'name' => 'emaildiubah',
    //         'email' => 'email888@mail.com'
    //     ]);

    //     $response->dump();

    //     $response
    //         ->assertStatus(200)
    //         ->assertJson([
    //             'message' => 'Data has been updated',
    //         ]);
    // }

    // public function testDelete()
    // {
    //     $response = $this
    //         ->actingAs(User::first())
    //         ->deleteJson('/api/user/6');

    //     $response->dump();

    //     $response
    //         ->assertStatus(200)
    //         ->assertJson([
    //             'message' => 'Data has been deleted',
    //         ]);
    // }
}
