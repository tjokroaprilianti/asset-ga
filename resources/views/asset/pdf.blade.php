<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Asset</title>
    <style media="all">
        body {
            /* font-family: 'Times New Roman', Times, serif; */
            font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
        }

        p, li {
            text-align: justify;
            line-height:1.5em
        }

        table {
            border-spacing: 5px;
            border-collapse: collapse;
        }

        table.item-list {
            width: 100%;
            margin-top: 30px;
            margin-bottom: 30px;
            font-size: 10px;
        }

        table.item-list > thead > tr > th {
            border: 1px solid #555;
            background-color: #ddd;
        }

        table.item-list > tbody > tr > td {
            border: 1px solid #555;
            border-top: none;
            padding: 3px 5px;
        }

        .text-center {
            text-align: center;
        }
    </style>
</head>
<body>
    <table class="item-list">
        <thead>
            <tr>
                <th>#</th>
                <th>Tag Number</th>
                <th>Code Category</th>
                <th>Name</th>
                <th>Category</th>
                <th>Trademark</th>
                <th>Model</th>
                <th>Qty</th>
                <th>Lifetime</th>
                <th>Year</th>
                <th>Location</th>
                <th>Unit</th>
                <th>Uselife</th>
                <th>Type</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $index => $row)
            <tr>
                <td>{{$index+1}}</td>
                <td>{{$row['Tag Number']}}</td>
                <td>{{$row['Code Category']}}</td>
                <td>{{$row['Name']}}</td>
                <td>{{$row['Category']}}</td>
                <td>{{$row['Trademark']}}</td>
                <td>{{$row['Model']}}</td>
                <td>{{$row['Qty']}}</td>
                <td>{{$row['Lifetime']}}</td>
                <td>{{$row['Year']}}</td>
                <td>{{$row['Location']}}</td>
                <td>{{$row['Unit']}}</td>
                <td>{{$row['Uselife']}}</td>
                <td>{{$row['Type']}}</td>
                <td>{{$row['Status']}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
