<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>BERITA ACARA</title>
    <style media="all">
        body {
            /* font-family: 'Times New Roman', Times, serif; */
            font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
        }

        p, li {
            text-align: justify;
            line-height:1.5em
        }

        table {
            border-spacing: 5px;
            border-collapse: collapse;
        }

        table.item-list {
            width: 100%;
            margin-top: 30px;
            margin-bottom: 30px;
            font-size: 10px;
        }

        table.item-list > thead > tr > th {
            border: 1px solid #555;
            background-color: #ddd;
        }

        table.item-list > tbody > tr > td {
            border: 1px solid #555;
            border-top: none;
        }

        .text-center {
            text-align: center;
        }
    </style>
</head>
<body>

    <h2 style="text-decoration:underline;text-align:center;margin-bottom:0">BERTA ACARA SERAH TERIMA</h2>
    <div style="text-align: center">Nomor: {{$assetMovement->number}}</div>

    <p>
        Pada hari  {{$assetMovement->hari}} tanggal {{$assetMovement->tanggal}} bulan {{$assetMovement->bulan}} tahun {{$assetMovement->tahun}} ({{$assetMovement->date}}) telah dilakukan serah terima barang berupa {{$assetMovement->quantity}} <strong>({{$assetMovement->jumlah}})</strong> unit alat penunjang kerja Operasional milik Aset PT Angkasa Pura Solusi dengan detail sebagai berikut:
    </p>

    <h4>I. SERAH TERIMA BARANG</h4>

    <p>Yang bertanda tangan di bawah ini:</p>

    <ul type="a">
        <li>
            <table>
                <tbody>
                    <tr>
                        <td style="width: 70px">Nama</td>
                        <td>: {{$assetMovement->pihak1['nama']}}</td>
                    </tr>
                    <tr>
                        <td style="width: 70px">Jabatan</td>
                        <td>: {{$assetMovement->pihak1['jabatan']}}</td>
                    </tr>
                    <tr>
                        <td style="width: 70px">Alamat</td>
                        <td>: {!! nl2br($assetMovement->pihak1['alamat']) !!}</td>
                    </tr>
                </tbody>
            </table>

            <p>Selanjutnya disebut <strong>PIHAK PERTAMA</strong> </p>
        </li>

        <li>
            <table>
                <tbody>
                    <tr>
                        <td style="width: 70px">Nama</td>
                        <td>: {{$assetMovement->pihak2['nama']}}</td>
                    </tr>
                    <tr>
                        <td style="width: 70px">Jabatan</td>
                        <td>: {{$assetMovement->pihak2['jabatan']}}</td>
                    </tr>
                    <tr>
                        <td style="width: 70px">Alamat</td>
                        <td>: {!! nl2br($assetMovement->pihak2['alamat']) !!}</td>
                    </tr>
                </tbody>
            </table>

            <p>Selanjutnya disebut <strong>PIHAK KEDUA</strong> </p>
        </li>

        <li>
            <strong>PIHAK PERTAMA</strong> menyerahkan barang sebanyak {{$assetMovement->quantity}} ({{$assetMovement->jumlah}}) unit dengan kondisi <strong>BAIK</strong> dan dapat digunakan dengan normal kepada <strong>PIHAK KEDUA</strong> dengan spesifikasi sebagai berikut:

            <table class="item-list">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>NAMA BARANG</th>
                        <th>MERK</th>
                        <th>PRODUCT S/N</th>
                        <th>MODEL/WARNA</th>
                        <th>JUMLAH</th>
                        <th>KONDISI</th>
                        <th>ASSET</th>
                        <th>LOKASI</th>
                        <th>KETERANGAN</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($assetMovement->items as $i => $item)
                    <tr>
                        <td class="text-center">{{$i+1}}.</td>
                        <td>{{$item->asset->name}}</td>
                        <td class="text-center">{{$item->trademark}}</td>
                        <td class="text-center">{{$item->serial_number}}</td>
                        <td class="text-center">{{$item->model}} /{{$item->color}}</td>
                        <td class="text-center">{{$item->qty}}</td>
                        <td class="text-center">{{$item->status->name}}</td>
                        <td class="text-center">APS</td>
                        <td class="text-center">{{$item->location->name}}</td>
                        <td class="text-center">{{$item->remarks}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </li>

        <li>
            Merujuk hal tersebut di atas, dengan ditandatanganinya berita acara ini maka sebagaimana tersebut pada huruf C di atas <em><strong>sesuai dan diserahkan</strong></em> ke <strong>PIHAK KEDUA</strong>, dan apabila terdapat kerusakan, kehilangan serta segala resiko yang timbul setelahnya menjadi tanggung jawab <strong>PIHAK KEDUA</strong>.
        </li>
    </ul>

    <h4>II. PENUTUP</h4>

    <p> Demikian Berita Acara Serah Terima ini dibuat untuk dipergunakan sebagaimana mestinya </p>

    <table style="width:100%;margin-top:50px;">
        <tbody>
            <tr>
                <td style="width:50%;text-align:center;">
                    Diserahkan Oleh: <br>
                    <strong>PIHAK PERTAMA</strong>

                    <div style="margin-top:100px;text-decoration:underline">{{strtoupper($assetMovement->pihak1['nama'])}}</div>
                </td>
                <td style="width:50%;text-align:center;">
                    Diterima Oleh: <br>
                    <strong>PIHAK KEDUA</strong>

                    <div style="margin-top:100px;text-decoration:underline">{{strtoupper($assetMovement->pihak2['nama'])}}</div>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
