<!DOCTYPE html>
<html lang="en">
    <head>
            <meta charset="utf-8">
        
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Asset QR Code</title>
        
    </head>
    <style>
        
        @media print {

        
            .flex-container {
                display: flex;
                flex-direction: column;
                /* padding-left:-150px; */
                
                /* margin-top:200; */
            }
            .flex-container > div {
                background-color: #f1f1f1;
                /* text-align: center; */
                /* line-height: 60px; */
                /* font-size: 10px; */
                /* margin-top :5px; */
                /* margin-bottom :10px; */
                padding-bottom:14px;
                
                
            }
            
            .grid-container {
                display: grid;
                grid-template-columns: 81px 81px;
                background-color: #2196F3;
               
                
            }
            .aw {
                
                background-color: rgba(255, 255, 255);
                border: 1px solid rgba(255, 255, 255);
                text-align:center ;
                
                
                
             }
             .ah {
                
                background-color: rgba(255, 255, 255);
                border: 1px solid rgba(255, 255, 255);
                text-align:left;
                padding-top:20px
                
             }
        }

            
             
    </style>
    <body>
        <div class="flex-container">
            @foreach ($assets as $asset)
            <div>
                <div class="grid-container">
                    <div class="grid-item aw" >
                        <svg width="70" height="70">
                             <!-- svg graphics elements -->
                             {!!$asset->qrCode!!}
                        </svg>
                    </div>
                    <div class="grid-item ah">
                            <strong>PT.APS</strong>
                            <br>
                            <small>{{$asset->unit ? $asset->unit->code : ''}}</small>
                    </div>
                </div>
            </div>
            @endforeach
  

            
          
        </div>
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="{{ asset('assets/printThis.js') }}"></script> -->
        <script>
            window.print()
        </script>
        <!-- <script>    
            $(document).ready(function(){
                $("print").click(function(){
                    alert("dd");
                });
            });
            $('selector').printThis();
        </script> -->
    </body>
</html>