<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Asset QR Code</title>
    </head>
    <style media="all">
        .qrcode-container {
            font-family: 'Arial';
            max-height: 20mm;
            min-height: 20mm;
            max-width: 40mm;
            min-width: 40mm;
            height: 20mm;
            width: 40mm;
            margin: 2mm auto 2mm;
            padding: 2mm;
            border: 1px solid #ccc;
            border-radius: 2mm;
            display: block;
            page-break-after: always;
        }
    </style>
    <body>
        @foreach ($assets as $asset)
        <div class="qrcode-container">
            <table>
                <tbody>
                    <tr>
                        <td>
                            {!!$asset->qrCode!!}<br>
                        </td>
                        <td>
                            <p>PT APS</p><br>
                            <small>{{$asset->unit ? $asset->unit->code : ''}}</small>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        @endforeach

        <script>
            window.print()
        </script>
    </body>
</html>
