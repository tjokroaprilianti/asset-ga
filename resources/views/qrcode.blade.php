<!DOCTYPE html>
<html lang="en">
    <head>
            <meta charset="utf-8">
        
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Asset QR Code</title>
        
    </head>
    <style>
        
        @media print {

        
            .flex-container {
                display: flex;
                flex-direction: column;
                /* padding-left:-150px; */
                
                /* margin-top:200; */
                padding-left:20px;
                
            }
            .flex-container > div {
                background-color: #f1f1f1;
                /* text-align: center; */
                /* line-height: 60px; */
                /* font-size: 10px; */
                /* margin-top :10px;
                margin-bottom :27px */
                
                padding-top :9px;
                padding-bottom :22px
                
                
                
            }
            
            .grid-container {
                display: grid;
                grid-template-columns: 80px 85px;
                   
                
            }
            .aw {
                
                background-color: rgba(255, 255, 255);
                border: 1px solid rgba(255, 255, 255);
                text-align:center ;
                
                
                
                
             }
             .ah {
                
                background-color: rgba(255, 255, 255);
                border: 1px solid rgba(255, 255, 255);
                font-family: 'Arial';
                text-align:right;
                padding-top:30px
                
             }
             /* #name{
                display:none;
             } */
        }

            
             
    </style>
    <body>
        <div class="flex-container">
            @foreach ($assets as $asset)
            <div>
                <div class="grid-container">
                    <div class="grid-item aw" >
                        <svg width="110" height="76">
                             <!-- svg graphics elements -->
                             {!!$asset->qrCode!!}
                        </svg>
                    </div>
                    <div class="grid-item ah">
                            <strong>PT.APS</strong>
                            <br>
                            <!-- <small id="name">{{$asset->name}}</small> -->
                    </div>
                </div>
            </div>
            @endforeach
  

            
          
        </div>
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="{{ asset('assets/printThis.js') }}"></script> -->
        <script>
            window.print()
        </script>
        <!-- <script>    
            $(document).ready(function(){
                $("print").click(function(){
                    alert("dd");
                });
            });
            $('selector').printThis();
        </script> -->
    </body>
</html>