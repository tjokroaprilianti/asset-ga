<?php

use App\Http\Controllers\AssetController;
use App\Http\Controllers\AssetMovementController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('asset/label', [AssetController::class, 'label']);
Route::get('assetMovement/printBeritaAcara/{assetMovement}', [AssetMovementController::class, 'printBeritaAcara']);
