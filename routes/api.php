<?php

use App\Http\Controllers\AssetCategoryController;
use App\Http\Controllers\AssetCoaController;
use App\Http\Controllers\AssetController;
use App\Http\Controllers\AssetMovementController;
use App\Http\Controllers\AssetStatusController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CostCenterController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserGroupController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SidebarMenuController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\VendorController;
use App\Http\Controllers\ProvinsiController;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [AuthController::class, 'login']);
Route::post('logout', [AuthController::class, 'logout']);

Route::post('token', function (Request $request) {
    $request->validate([
        'email' => 'required|email',
        'password' => 'required',
        'device_name' => 'required',
    ]);

    $user = User::where('email', $request->email)->first();

    if (!$user || !Hash::check($request->password, $user->password)) {
        throw ValidationException::withMessages([
            'email' => ['The provided credentials are incorrect.'],
        ]);
    }

    return [
        'token' => $user->createToken($request->device_name)->plainTextToken,
        'user' => $user
    ];
});

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::get('me', [AuthController::class, 'me']);
    Route::resource('user', UserController::class)->except(['create', 'edit']);
    Route::resource('userGroup', UserGroupController::class)->except(['create', 'edit']);
    Route::post('asset/import', [AssetController::class, 'import']);
    Route::get('asset/export', [AssetController::class, 'export']);
    Route::resource('asset', AssetController::class)->except(['create', 'edit']);
    Route::post('assetCategory/import', [AssetCategoryController::class, 'import']);
    Route::resource('assetCategory', AssetCategoryController::class)->except(['create', 'edit']);
    Route::resource('costCenter', CostCenterController::class)->except(['create', 'edit']);
    Route::resource('assetMovement', AssetMovementController::class)->except(['create', 'edit']);
    Route::resource('assetStatus', AssetStatusController::class)->except(['create', 'edit']);
    Route::resource('vendor', VendorController::class)->except(['create', 'edit']);
    Route::resource('location', LocationController::class)->except(['create', 'edit']);
    Route::resource('unit', UnitController::class)->except(['create', 'edit']);
    Route::resource('assetCoa', AssetCoaController::class)->except(['create', 'edit']);

    Route::get('sidebarMenu', [SidebarMenuController::class, 'index']);
    Route::post('upload', [AssetController::class, 'upload']);

    Route::get('unitList', [UnitController::class, 'getList']);
    Route::get('locationList', [LocationController::class, 'getList']);
    Route::get('vendorList', [VendorController::class, 'getList']);

});
Route::get('provinsiList', [ProvinsiController::class, 'getList']);

Route::get('asset/qrCode/{asset}', [AssetController::class, 'qrCode']);
Route::get('report', [ReportController::class, 'index']);
Route::get('report/piechart1', [ReportController::class, 'piechart1']);
Route::get('report/piechart2', [ReportController::class, 'piechart2']);
Route::get('report/barchart', [ReportController::class, 'barchart']);
Route::get('report/barchart2', [ReportController::class, 'barchart2']);
Route::get('report/boxchart', [ReportController::class, 'boxchart']);
Route::get('report/areachart', [ReportController::class, 'areachart']);
Route::get('report/stackedchart', [ReportController::class, 'stackedchart']);
Route::get('report/mapcart', [ReportController::class, 'mapcart']);
