import { resolve } from 'path'

// eslint-disable-next-line import/no-absolute-path
import original from '/home/udibagas/apps/ap-asset/resources/nuxt/nuxt.config'

const config = { ...original }

config.mode = 'spa'
config.rootDir = resolve('/home/udibagas/apps/ap-asset/resources/nuxt')
config.modules = [...(config.modules || []), 'nuxt-laravel']
config.laravel = {
  dotEnvExport: true
}

config.router = {
  ...config.router,
  base: '/app/'
}

export default config
