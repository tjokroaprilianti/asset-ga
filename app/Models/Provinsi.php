<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    use HasFactory;

    protected $fillable = [
        'city',
        'id',
        'latlngs',
        ''
    ];

    protected $casts = ['coordinates' => 'json'];
    protected $appends = ['quantity'];
    protected $with = ['location'];


    public function location()
    {
        return $this->hasMany(Location::class ,'provinsi_id', 'id');
    }

    public function getQuantityAttribute()
    {

        return  $this->location->sum('qtyasset');

    }
}
