<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssetMovementItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'asset_movement_id',
        'asset_id',
        'trademark',
        'serial_number',
        'model',
        'qty',
        'status_id',
        'location_id',
        'remarks'
    ];

    public function asset()
    {
        return $this->belongsTo(Asset::class);
    }

    public function status()
    {
        return $this->belongsTo(AssetStatus::class, 'status_id');
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }
}
