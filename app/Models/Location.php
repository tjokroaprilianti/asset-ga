<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'code', 'description', 'parent_id', 'lat', 'lng','provinsi_id'
    ];

    protected $with = ['children'];
    protected $appends = ['qtyasset'];

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }
    public function asset()
    {
        return $this->hasMany(Asset::class);
    }
    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class);
    }
    public function getQtyassetAttribute()
    {
        //$ss = $this->location->get() ;
       //dd($ss);
        return  $this->asset->sum('qty');
            //dd($tes[5]);


    }
}
