<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Terbilang;
use Carbon\Carbon;

class AssetMovement extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'date',
        'note',
        'number',
        'pihak1',
        'pihak2'
    ];

    protected $casts = [
        'pihak1' => 'json',
        'pihak2' => 'json',
    ];

    protected $with = ['items'];

    protected $appends = ['berita_acara_link','tahun','bulan','tanggal','hari','quantity','jumlah'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function items()
    {
        return $this->hasMany(AssetMovementItem::class);
    }

    public function getBeritaAcaraLinkAttribute()
    {
        return env('APP_URL') . '/assetMovement/printBeritaAcara/' . $this->id;
    }
    public function getTahunAttribute()
    {
      
        return Terbilang::make(Carbon::parse($this->created_at)->format('Y'));
    }
    public function getBulanAttribute()
    {
        
        return Terbilang::make(Carbon::parse($this->created_at)->format('m'));
    }
    public function getTanggalAttribute()
    {
        
        return Terbilang::make(Carbon::parse($this->created_at)->format('d'));
    }
    public function getHariAttribute()
    {
       
        return Carbon::parse($this->created_at)->isoFormat('dddd');
    }
    public function getQuantityAttribute()
    {

        return $this->items->where('asset_movement_id',$this->id)->sum('qty');
        
    }
    public function getJumlahAttribute()
    {

        return Terbilang::make($this->quantity);
        
    }
}
