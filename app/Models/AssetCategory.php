<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssetCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'golongan', 'bidang', 'kelompok', 'sub_kelompok', 'sub_sub_kelompok', 'name', 'description'
    ];
    protected $appends = ['code'];

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }
    public function asset()
    {
        return $this->hasMany(Asset::class);
    }
    public function getCodeAttribute()
    {
        
        return  $this->attributes['golongan'] . '.'
                .$this->attributes['bidang']. '.'
                .$this->attributes['kelompok']. '.'
                .$this->attributes['sub_kelompok']. '.'
                .$this->attributes['sub_sub_kelompok'];
    }
}
