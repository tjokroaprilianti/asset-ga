<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use LaravelQRCode\Facades\QRCode;
use Carbon\Carbon;

class Asset extends Model
{
    use HasFactory;

    protected $fillable = [
        'registration_number',
        'name',
        'trademark',
        'model',
        'color',
        'serial_number',
        'part_number',
        'lifetime',
        'price',
        'value',
        'year',
        'type',
        'uselife',
        'asset_category_id',
        'vendor_id',
        'location_id',
        'unit_id',
        'status_id',
        'picture',
        'qty',
        'remark',
        'created_at',
        'invoice_number',
        'tag_no'
    ];

    protected $appends = ['picture_url', 'qr_code_url', 'label_url'];

    public function assetCategory()
    {
        return $this->belongsTo(AssetCategory::class);
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function status()
    {
        return $this->belongsTo(AssetStatus::class, 'status_id');
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function getPictureUrlAttribute()
    {
        if ($this->picture) {
            return url(Storage::url($this->picture));
        }

        return null;
    }

    public function getQrCodeAttribute()
    {
        return QRCode::text("ID: {$this->id}\nNAME: {$this->name}\nTRADEMARK: {$this->trademark}\nMODEL/COLOR:{$this->model} {$this->color}\nSN:{$this->serial_number}")
            ->setMargin(1)
            ->setSize(2)
            ->svg();
    }

    public function getQrCodeUrlAttribute()
    {
        return url('/api/asset/qrCode/' . $this->id);
    }

    public function getLabelUrlAttribute()
    {
        return url('/asset/label/' . $this->id);
    }

    public function getTagNumberAttribute()
    {
        $category = $this->assetCategory;

        if (!$category) {
            return '';
        }

        $group = $category->golongan . '.'
            . $category->bidang . '.'
            . $category->kelompok . '.'
            . $category->sub_kelompok . '.'
            . $category->sub_sub_kelompok . '.';

        $sameCategory = self::where('asset_category_id', $category->id)
            ->where('id', '<', $this->id)->count();

        return $group . (str_pad($sameCategory + 1, 6, '0', STR_PAD_LEFT));
    }

    protected static function booted()
    {
        static::creating(function ($asset) {
            $category = $asset->assetCategory;

            if (!$category) {
                return '';
            }

            $group = $category->golongan . '.'
                . $category->bidang . '.'
                . $category->kelompok . '.'
                . $category->sub_kelompok . '.'
                . $category->sub_sub_kelompok . '.';

            $sameCategory = self::where('asset_category_id', $category->id)->count();
            $asset->tag_no = $group . (str_pad($sameCategory + 1, 6, '0', STR_PAD_LEFT));
        });

        static::updating(function ($asset) {
            $category = $asset->assetCategory;

            if (!$category) {
                return '';
            }

            $group = $category->golongan . '.'
                . $category->bidang . '.'
                . $category->kelompok . '.'
                . $category->sub_kelompok . '.'
                . $category->sub_sub_kelompok . '.';

            $sameCategory = self::where('asset_category_id', $category->id)
                ->where('id', '<', $asset->id)->count();

            $asset->tag_no = $group . (str_pad($sameCategory + 1, 6, '0', STR_PAD_LEFT));
        });
    }
}
