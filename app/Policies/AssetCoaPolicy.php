<?php

namespace App\Policies;

use App\Models\AccessControl;
use App\Models\AssetCoa;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AssetCoaPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return AccessControl::check($user, AssetCoa::class, 'viewAny');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AssetCoa  $assetCoa
     * @return mixed
     */
    public function view(User $user, AssetCoa $assetCoa)
    {
        return AccessControl::check($user, AssetCoa::class, 'view');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return AccessControl::check($user, AssetCoa::class, 'create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AssetCoa  $assetCoa
     * @return mixed
     */
    public function update(User $user, AssetCoa $assetCoa)
    {
        return AccessControl::check($user, AssetCoa::class, 'update');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AssetCoa  $assetCoa
     * @return mixed
     */
    public function delete(User $user, AssetCoa $assetCoa)
    {
        return AccessControl::check($user, AssetCoa::class, 'delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AssetCoa  $assetCoa
     * @return mixed
     */
    public function restore(User $user, AssetCoa $assetCoa)
    {
        return AccessControl::check($user, AssetCoa::class, 'restore');
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AssetCoa  $assetCoa
     * @return mixed
     */
    public function forceDelete(User $user, AssetCoa $assetCoa)
    {
        return AccessControl::check($user, AssetCoa::class, 'forceDelete');
    }
}
