<?php

namespace App\Policies;

use App\Models\AccessControl;
use App\Models\Provinsi;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProvinsiPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return AccessControl::check($user, Provinsi::class, 'viewAny');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Provinsi  $provinsi
     * @return mixed
     */
    public function view(User $user, Provinsi $provinsi)
    {
        return AccessControl::check($user, Provinsi::class, 'view');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return AccessControl::check($user, Provinsi::class, 'create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Provinsi  $provinsi
     * @return mixed
     */
    public function update(User $user, Provinsi $provinsi)
    {
        return AccessControl::check($user, Provinsi::class, 'update');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Provinsi  $provinsi
     * @return mixed
     */
    public function delete(User $user, Provinsi $provinsi)
    {
        return AccessControl::check($user, Provinsi::class, 'delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Provinsi  $provinsi
     * @return mixed
     */
    public function restore(User $user, Provinsi $provinsi)
    {
        return AccessControl::check($user, Provinsi::class, 'restore');
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Provinsi  $provinsi
     * @return mixed
     */
    public function forceDelete(User $user, Provinsi $provinsi)
    {
        return AccessControl::check($user, Provinsi::class, 'forceDelete');
    }
}
