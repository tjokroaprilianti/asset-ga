<?php

namespace App\Policies;

use App\Models\AccessControl;
use App\Models\AssetCategory;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AssetCategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return AccessControl::check($user, AssetCategory::class, 'viewAny');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AssetCategory  $assetCategory
     * @return mixed
     */
    public function view(User $user, AssetCategory $assetCategory)
    {
        return AccessControl::check($user, AssetCategory::class, 'view');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return AccessControl::check($user, AssetCategory::class, 'create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AssetCategory  $assetCategory
     * @return mixed
     */
    public function update(User $user, AssetCategory $assetCategory)
    {
        return AccessControl::check($user, AssetCategory::class, 'update');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AssetCategory  $assetCategory
     * @return mixed
     */
    public function delete(User $user, AssetCategory $assetCategory)
    {
        return AccessControl::check($user, AssetCategory::class, 'delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AssetCategory  $assetCategory
     * @return mixed
     */
    public function restore(User $user, AssetCategory $assetCategory)
    {
        return AccessControl::check($user, AssetCategory::class, 'restore');
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AssetCategory  $assetCategory
     * @return mixed
     */
    public function forceDelete(User $user, AssetCategory $assetCategory)
    {
        return AccessControl::check($user, AssetCategory::class, 'forceDelete');
    }
}
