<?php

namespace App\Policies;

use App\Models\AccessControl;
use App\Models\AssetStatus;
use App\Models\Authorization;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AssetStatusPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return AccessControl::check($user, AssetStatus::class, 'viewAny');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AssetStatus  $assetStatus
     * @return mixed
     */
    public function view(User $user, AssetStatus $assetStatus)
    {
        return AccessControl::check($user, AssetStatus::class, 'view');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return AccessControl::check($user, AssetStatus::class, 'create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AssetStatus  $assetStatus
     * @return mixed
     */
    public function update(User $user, AssetStatus $assetStatus)
    {
        return AccessControl::check($user, AssetStatus::class, 'update');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AssetStatus  $assetStatus
     * @return mixed
     */
    public function delete(User $user, AssetStatus $assetStatus)
    {
        return AccessControl::check($user, AssetStatus::class, 'delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AssetStatus  $assetStatus
     * @return mixed
     */
    public function restore(User $user, AssetStatus $assetStatus)
    {
        return AccessControl::check($user, AssetStatus::class, 'restore');
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AssetStatus  $assetStatus
     * @return mixed
     */
    public function forceDelete(User $user, AssetStatus $assetStatus)
    {
        return AccessControl::check($user, AssetStatus::class, 'forceDelete');
    }
}
