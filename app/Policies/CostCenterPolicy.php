<?php

namespace App\Policies;

use App\Models\AccessControl;
use App\Models\CostCenter;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CostCenterPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return AccessControl::check($user, CostCenter::class, 'viewAny');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\CostCenter  $costCenter
     * @return mixed
     */
    public function view(User $user, CostCenter $costCenter)
    {
        return AccessControl::check($user, CostCenter::class, 'view');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return AccessControl::check($user, CostCenter::class, 'create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\CostCenter  $costCenter
     * @return mixed
     */
    public function update(User $user, CostCenter $costCenter)
    {
        return AccessControl::check($user, CostCenter::class, 'update');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\CostCenter  $costCenter
     * @return mixed
     */
    public function delete(User $user, CostCenter $costCenter)
    {
        return AccessControl::check($user, CostCenter::class, 'delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\CostCenter  $costCenter
     * @return mixed
     */
    public function restore(User $user, CostCenter $costCenter)
    {
        return AccessControl::check($user, CostCenter::class, 'restore');
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\CostCenter  $costCenter
     * @return mixed
     */
    public function forceDelete(User $user, CostCenter $costCenter)
    {
        return AccessControl::check($user, CostCenter::class, 'forceDelete');
    }
}
