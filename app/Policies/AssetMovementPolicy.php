<?php

namespace App\Policies;

use App\Models\AccessControl;
use App\Models\AssetMovement;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AssetMovementPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return AccessControl::check($user, AssetMovement::class, 'viewAny');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AssetMovement  $assetMovement
     * @return mixed
     */
    public function view(User $user, AssetMovement $assetMovement)
    {
        return AccessControl::check($user, AssetMovement::class, 'view');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return AccessControl::check($user, AssetMovement::class, 'create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AssetMovement  $assetMovement
     * @return mixed
     */
    public function update(User $user, AssetMovement $assetMovement)
    {
        return AccessControl::check($user, AssetMovement::class, 'update');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AssetMovement  $assetMovement
     * @return mixed
     */
    public function delete(User $user, AssetMovement $assetMovement)
    {
        return AccessControl::check($user, AssetMovement::class, 'delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AssetMovement  $assetMovement
     * @return mixed
     */
    public function restore(User $user, AssetMovement $assetMovement)
    {
        return AccessControl::check($user, AssetMovement::class, 'restore');
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AssetMovement  $assetMovement
     * @return mixed
     */
    public function forceDelete(User $user, AssetMovement $assetMovement)
    {
        return AccessControl::check($user, AssetMovement::class, 'forceDelete');
    }
}
