<?php

namespace App\Http\Controllers;

use App\Http\Requests\CostCenterRequest;
use App\Http\Resources\CostCenterCollection;
use App\Models\CostCenter;
use Illuminate\Http\Request;

class CostCenterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', CostCenter::class);

        return new CostCenterCollection(
            CostCenter::when($request->keyword, function ($q) use ($request) {
                $q->where(function ($q) use ($request) {
                    $q->where('code', 'ILIKE', "%{$request->keyword}%")
                        ->orWhere('name', 'ILIKE', "%{$request->keyword}%");
                });
            })->orderBy(
                $request->sort_field ?: 'name',
                $request->sort_direction == 'descending' ? 'desc' : 'asc'
            )->paginate($request->per_page)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CostCenterRequest $request)
    {
        $this->authorize('create', CostCenter::class);
        $costCenter = CostCenter::create($request->all());
        return ['message' => 'Data has been saved', 'data' => $costCenter];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CostCenter  $costCenter
     * @return \Illuminate\Http\Response
     */
    public function show(CostCenter $costCenter)
    {
        $this->authorize('view', $costCenter);
        return $costCenter;
    }

    /**
     * Update the specifphp artisan stub:publishied resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CostCenter  $costCenter
     * @return \Illuminate\Http\Response
     */
    public function update(CostCenterRequest $request, CostCenter $costCenter)
    {
        $this->authorize('update', $costCenter);
        $costCenter->update($request->all());
        return ['message' => 'Data has been updated', 'data' => $costCenter];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CostCenter  $costCenter
     * @return \Illuminate\Http\Response
     */
    public function destroy(CostCenter $costCenter)
    {
        $this->authorize('delete', $costCenter);
        $costCenter->delete();
        return ['message' => 'Data has been deleted'];
    }
}
