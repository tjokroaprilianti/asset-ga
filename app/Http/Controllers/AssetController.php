<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Http\Requests\AssetRequest;
use App\Http\Resources\AssetCollection;
use App\Models\AssetCategory;
use App\Models\AssetStatus;
use App\Models\Location;
use App\Models\Unit;
use App\Models\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Storage;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $this->authorize('viewAny', Asset::class);
        return new AssetCollection(
            Asset::when($request->keyword, function ($q) use ($request) {
                $q->where(function ($q) use ($request) {
                    $q->where('name', 'ILIKE', "%{$request->keyword}%")
                        ->orWhere('serial_number', 'ILIKE', "%{$request->keyword}%")
                        ->orWhere('part_number', 'ILIKE', "%{$request->keyword}%")
                        ->orWhere('model', 'ILIKE', "%{$request->keyword}%")
                        ->orWhere('trademark', 'ILIKE', "%{$request->keyword}%")
                        ->orWhere('tag_no', 'ILIKE', "%{$request->keyword}%")
                        ->orWhereHas('location', function ($q) use ($request) {
                            $q->where('name', 'ILIKE', "%{$request->keyword}%");
                        })
                        ->orWhereHas('vendor', function ($q) use ($request) {
                            $q->where('name', 'ILIKE', "%{$request->keyword}%");
                        })
                        ->orWhereHas('unit', function ($q) use ($request) {
                            $q->where('name', 'ILIKE', "%{$request->keyword}%");
                        })
                        ->orWhereHas('assetCategory', function ($q) use ($request) {
                            $q->where('name', 'ILIKE', "%{$request->keyword}%");
                        });
                });
            })->when($request->unit_id, function ($q) use ($request) {
                $q->whereIn('unit_id', $request->unit_id);
            })->when($request->location_id, function ($q) use ($request) {
                $q->whereIn('location_id', $request->location_id);
            })->when($request->asset_category_id, function ($q) use ($request) {
                $q->whereIn('asset_category_id', $request->asset_category_id);
            })->when($request->status_id, function ($q) use ($request) {
                $q->whereIn('status_id', $request->status_id);
            })->orderBy(
                $request->sort_field ?: 'name',
                $request->sort_direction == 'descending' ? 'desc' : 'asc'
            )->paginate($request->per_page)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AssetRequest $request)
    {
        $this->authorize('create', Asset::class);
        $asset = Asset::create($request->all());
        return ['message' => 'Data has been saved', 'data' => $asset];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show(Asset $asset)
    {
        // $this->authorize('view', $asset);
        return $asset->append('tag_number')->load(['location', 'assetCategory', 'vendor', 'unit', 'status']);
    }

    /**
     * Update the specifphp artisan stub:publishied resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function update(AssetRequest $request, Asset $asset)
    {
        $this->authorize('update', $asset);
        $asset->update($request->all());
        return ['message' => 'Data has been updated', 'data' => $asset];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset $asset)
    {
        $this->authorize('delete', $asset);
        $asset->delete();
        return ['message' => 'Data has been deleted'];
    }

    public function upload(Request $request)
    {
        $request->validate(['file' => 'required']);

        $path = $request->file('file')->store('public/' . date('Y/m/d'));

        return [
            'message' => 'Data has been uploaded',
            'path' => $path,
            'url' => url(Storage::url($path))
        ];
    }

    public function qrCode(Asset $asset)
    {
        return $asset->qrCode;
    }

    public function import(Request $request)
    {

        $request->validate([
            'rows' => 'required'
        ]);

        $response = DB::transaction(function () use ($request) {
            foreach ($request->rows as $row) {

                // find location

                $location = Location::firstOrCreate(['name' => $row['location']], [
                    'name' => $row['location'],
                    'code' => '-',
                ]);

                $unit = Unit::firstOrCreate(['name' => $row['unit']], [
                    'name' => $row['unit'],
                    'code' => '-',
                    'cost_center_code' => '-'
                ]);

                if ($row['vendor']) {
                    $vendor = Vendor::firstOrCreate(['name' => $row['vendor']], [
                        'name' => $row['vendor'],
                        'code' => '-',
                        'cost_center_code' => '-'
                    ]);
                } else {
                    $vendor = null;
                }

                $status = AssetStatus::firstOrCreate(
                    ['name' => $row['status']],
                    [
                        'name' => $row['status'],
                        'code' => strtoupper(substr($row['status'], 0, 2))
                    ]
                );


                if ($row['category']) {
                    $code = explode('.', $row['category']);
                    //return($code);
                    if (count($code) == 5) {
                        $category = AssetCategory::where('golongan', $code[0])
                            ->where('bidang', $code[1])
                            ->where('kelompok', $code[2])
                            ->where('sub_kelompok', $code[3])
                            ->where('sub_sub_kelompok', $code[4])
                            ->first();
                    }
                } else {
                    $category = null;
                }

                $data = [
                    'name' => $row['name'],
                    'asset_category_id' => $category ? $category->id : null,
                    'trademark' => $row['trademark'],
                    'model' => $row['model'],
                    'color' => $row['color'],
                    'serial_number' => $row['serial_number'],
                    'part_number' => $row['part_number'],
                    'qty' => $row['qty'],
                    'price'   => $row['price'],
                    'invoice_number' => $row['invoice_number'],
                    'value'   => $row['value'],
                    'lifetime'   => $row['lifetime'],
                    'year'   => $row['year'],
                    'location_id' => $location->id,
                    'unit_id' => $unit->id,
                    'uselife'   => $row['uselife'],
                    'type' => 'OWNED',
                    'vendor_id' => $vendor ? $vendor->id : null,
                    'status_id' => $status->id,
                    'remark' => $row['remark'],

                ];

                $asset = Asset::updateOrCreate([
                    'name' => $row['name'],
                    'trademark' => $row['trademark'],
                    'serial_number' => $row['serial_number'],
                    'location_id' => $location->id,
                    'remark' => $row['remark'],
                    'unit_id' => $unit->id,
                    'lifetime'   => $row['lifetime'],
                ], $data);
            }

            return response(['message' => 'Data has been imported successfully'], 201);
        });

        return $response;
    }

    public function label(Request $request)
    {
        $assets = Asset::when($request->id, function ($q) use ($request) {
            $q->where('id', $request->id);
        })->when($request->keyword, function ($q) use ($request) {
            $q->where(function ($q) use ($request) {
                $q->where('name', 'ILIKE', "%{$request->keyword}%")
                    ->orWhere('serial_number', 'ILIKE', "%{$request->keyword}%")
                    ->orWhere('part_number', 'ILIKE', "%{$request->keyword}%")
                    ->orWhere('model', 'ILIKE', "%{$request->keyword}%")
                    ->orWhere('trademark', 'ILIKE', "%{$request->keyword}%")
                    ->orWhere('tag_no', 'ILIKE', "%{$request->keyword}%")
                    ->orWhereHas('location', function ($q) use ($request) {
                        $q->where('name', 'ILIKE', "%{$request->keyword}%");
                    })
                    ->orWhereHas('vendor', function ($q) use ($request) {
                        $q->where('name', 'ILIKE', "%{$request->keyword}%");
                    })
                    ->orWhereHas('unit', function ($q) use ($request) {
                        $q->where('name', 'ILIKE', "%{$request->keyword}%");
                    })
                    ->orWhereHas('assetCategory', function ($q) use ($request) {
                        $q->where('name', 'ILIKE', "%{$request->keyword}%");
                    });
            });
        })->when($request->unit_id, function ($q) use ($request) {
            $q->whereIn('unit_id', $request->unit_id);
        })->when($request->location_id, function ($q) use ($request) {
            $q->whereIn('location_id', $request->location_id);
        })->when($request->asset_category_id, function ($q) use ($request) {
            $q->whereIn('asset_category_id', $request->asset_category_id);
        })->when($request->status_id, function ($q) use ($request) {
            $q->whereIn('status_id', $request->status_id);
        })->orderBy(
            $request->sort_field ?: 'name',
            $request->sort_direction == 'descending' ? 'desc' : 'asc'
        )->paginate($request->per_page);

        return view('qrcode', ['assets' => $assets]);
    }

    public function export(Request $request)
    {
        $data = Asset::when($request->keyword, function ($q) use ($request) {
            $q->where(function ($q) use ($request) {
                $q->where('name', 'ILIKE', "%{$request->keyword}%")
                    ->orWhere('serial_number', 'ILIKE', "%{$request->keyword}%")
                    ->orWhere('part_number', 'ILIKE', "%{$request->keyword}%")
                    ->orWhere('model', 'ILIKE', "%{$request->keyword}%")
                    ->orWhere('trademark', 'ILIKE', "%{$request->keyword}%")
                    ->orWhere('tag_no', 'ILIKE', "%{$request->keyword}%")
                    ->orWhereHas('location', function ($q) use ($request) {
                        $q->where('name', 'ILIKE', "%{$request->keyword}%");
                    })
                    ->orWhereHas('vendor', function ($q) use ($request) {
                        $q->where('name', 'ILIKE', "%{$request->keyword}%");
                    })
                    ->orWhereHas('unit', function ($q) use ($request) {
                        $q->where('name', 'ILIKE', "%{$request->keyword}%");
                    })
                    ->orWhereHas('assetCategory', function ($q) use ($request) {
                        $q->where('name', 'ILIKE', "%{$request->keyword}%");
                    });
            });
        })->when($request->unit_id, function ($q) use ($request) {
            if (is_array($request->unit_id)) {
                $q->whereIn('unit_id', $request->unit_id);
            }
            $q->where('unit_id', $request->unit_id);
        })->when($request->location_id, function ($q) use ($request) {
            if (is_array($request->location_id)) {
                $q->whereIn('location_id', $request->location_id);
            }

            $q->where('location_id', $request->location_id);
        })->when($request->asset_category_id, function ($q) use ($request) {
            if (is_array($request->asset_category_id)) {
                $q->whereIn('asset_category_id', $request->asset_category_id);
            }

            $q->where('asset_category_id', $request->asset_category_id);
        })->when($request->status_id, function ($q) use ($request) {
            if (is_array($request->status_id)) {
                $q->whereIn('status_id', $request->status_id);
            }

            $q->where('status_id', $request->status_id);
        })->orderBy(
            $request->sort_field ?: 'name',
            $request->sort_direction == 'descending' ? 'desc' : 'asc'
        )->get()->map(function ($item, $index) {
            return [
                '#' => $index + 1,
                'Tag Number' => $item->tag_no,
                'Code Category' => $item->assetCategory ? $item->assetCategory->code : '-',
                'Name' => $item->name,
                'Category' => $item->assetCategory ? $item->assetCategory->name : '-',
                'Trademark' => $item->trademark ?: '',
                'Model' => $item->model ?: '',
                'Color' => $item->color ?: '',
                'Serial Number' => $item->serial_number ?: '',
                'Part Number' => $item->part_number ?: '',
                'Qty' => $item->qty,
                'Price' => $item->price ?: '',
                'Nomor Invoice' => $item->invoice_number ?: '',
                'Value' => $item->value ?: '',
                'Lifetime' => $item->lifetime ?: '',
                'Year' => $item->year ?: '',
                'Location' => $item->location ? $item->location->name : '-',
                'Unit' => $item->unit ? $item->unit->name : '-',
                'Uselife' => $item->uselife ?: '',
                'Type' => $item->type ?: '',
                'Vendor' => $item->vendor ? $item->vendor->name : '',
                'Status' => $item->status ? $item->status->name : '',
                'Remark' => $item->remark ?: '',
            ];
        });

        if ($request->type == 'pdf') {
            $pdf = PDF::loadView('asset.pdf', compact('data'))->setPaper('a4', 'landscape');
            return $pdf->stream();
        }

        return $data;
    }
}
