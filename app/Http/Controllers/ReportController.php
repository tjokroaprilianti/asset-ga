<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Models\AssetCategory;
use App\Models\Vendor;
use App\Models\Location;
use App\Models\Provinsi;
use App\Models\Unit;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class ReportController extends Controller
{
    public function index()
    {
        return Asset::selectRaw('COUNT(assets.id) AS jumlah, asset_statuses.name AS status')
            ->join('asset_statuses', 'asset_statuses.id', 'assets.status_id')
            ->groupBy('asset_statuses.name')
            ->get()->map(function ($item) {
                return [
                    'jumlah' => $item->jumlah,
                    'status' => $item->status
                ];
            });
    }

    public function piechart1()
    {
        return Asset::select('status_id', DB::raw('count(*) as total'))->groupBy('status_id')
            ->with('status')
            ->get()
            ->map(function ($key) {
                $status = isset($key->status->name) ? $key->status->name : 'UNDEFINED';
                return [
                    'name' => $status,
                    'y' => $key->total,
                ];
            });
    }

    public function piechart2()
    {
        return Asset::select('unit_id', DB::raw('count(*) as total'))->groupBy('unit_id')
            ->get()
            ->map(function ($key) {
                $unit = isset($key->unit->name) ? $key->unit->name : 'UNDEFINED';
                return [
                    'name' => $unit,
                    'y' => $key->total,
                ];
            });
    }

    public function barchart()
    {
        return Asset::select('asset_category_id', DB::raw('count(*) as total'))->groupBy('asset_category_id')
            ->orderBy('total', 'desc')
            ->get()
            ->map(function ($key) {
                return [
                    'name' => $key->assetCategory ? $key->assetCategory->name : 'Uncategorized',
                    'y' => $key->total,
                ];
            })->take(10);
    }

    public function barchart2()
    {
        return Asset::select('location_id', DB::raw('count(*) as total'))->groupBy('location_id')
            ->orderBy('total', 'desc')
            ->get()
            ->map(function ($key) {
                return [
                    'name' => $key->location->name,
                    'y' => $key->total,
                ];
            })
            ->take(20);
    }

    public function boxchart()
    {
        $asset = asset::get()->count();
        $vendor = Vendor::get()->count();
        $location = Location::get()->count();
        $unit = Unit::get()->count();

        return response([
            'asset' => $asset,
            'vendor' => $vendor,
            'location' => $location,
            'unit'      => $unit
        ]);
    }

    public function areachart()
    {
        return Asset::select('location_id')->groupBy('location_id')
            ->orderBy('location_id', 'asc')

            ->get()
            ->map(function ($key) {
                $as = asset::select('created_at',DB::raw('count(*) as total'))->where('location_id', $key->location_id)->groupBy('created_at')->get();
                $arr = [];
                for ($i = 0; $i < count($as); $i++) {
                    $arr[] = $as[$i]->total;
                }
                return [
                    'name' => $key->location->name,
                    'data' => $arr
                ];
            });
    }

    public function stackedchart()
    {
        $templates = ['1664','1659','1655','1709','1712']; //id category
        $darilocation = Asset::select('location_id',DB::raw('count(qty) as total'))
                ->groupBy('location_id')
                ->orderBy('total', 'desc')
                ->get()
                ->map(function ($key) use ($templates){
                    $itemCategry=[];
                    foreach($templates as $row){
                        $category = AssetCategory::find($row);

                        $total = Asset::select( DB::raw('count(qty) as qtytotal'))
                            ->where('asset_category_id',$row)
                            ->where('location_id',$key->location_id)
                            ->get();

                        $total = isset($total[0]->qtytotal)?$total[0]->qtytotal:0;

                        $itemCategry[]=[
                            'id'=>$row,
                            'categoryName'=>$category->name,
                            'qtytotal'=>$total
                        ];
                    }
                    return [
                        'location_id' => $key->location_id,
                        'location_name' => $key->location? $key->location->name : 'UNDEFINED',
                        'totalqty'    => $key->total,
                        'cat' => $itemCategry
                    ];
                })->take(5);
        return  $darilocation;
    }

    public function mapcart(){

        //return Provinsi::get();
        $prov = Provinsi::get()->filter(function($item) {
            return $item->quantity > 0;
        })->values()->map(function($key){
            return [
                'type' => 'Feature',
                'geometry' => collect([
                    'coordinates' => $key->coordinates,
                    'type' => $key->type
                ]),
                'properties' => collect([
                        'id' => $key->id,
                        'name' => $key->city,
                        'Shape_Leng' => $key->Shape_Leng,
                        'Shape_Area' => $key->Shape_Area,
                        'total'  => $key->quantity
                    ])
            ];
        });

        return response()->json([
            'susse' => true,
            'data' => $data = collect([
                    'type' => 'FeatureCollection',
                    'features' => $prov,
                ])
            ]
        );


    }
}
