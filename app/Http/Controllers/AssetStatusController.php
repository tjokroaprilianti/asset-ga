<?php

namespace App\Http\Controllers;

use App\Models\AssetStatus;
use App\Http\Requests\AssetStatusRequest;
use App\Http\Resources\AssetStatusCollection;
use Illuminate\Http\Request;

class AssetStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', AssetStatus::class);

        return new AssetStatusCollection(
            AssetStatus::when($request->keyword, function ($q) use ($request) {
                $q->where(function ($q) use ($request) {
                    $q->where('code', 'ILIKE', "%{$request->keyword}%")
                        ->orWhere('name', 'ILIKE', "%{$request->keyword}%")
                        ->orWhere('description', 'ILIKE', "%{$request->keyword}%");
                });
            })->orderBy(
                $request->sort_field ?: 'name',
                $request->sort_direction == 'descending' ? 'desc' : 'asc'
            )->paginate($request->per_page)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AssetStatusRequest $request)
    {
        $this->authorize('create', AssetStatus::class);
        $assetStatus = AssetStatus::create($request->all());
        return ['message' => 'Data has been saved', 'data' => $assetStatus];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AssetStatus  $assetStatus
     * @return \Illuminate\Http\Response
     */
    public function show(AssetStatus $assetStatus)
    {
        $this->authorize('view', $assetStatus);
        return $assetStatus;
    }

    /**
     * Update the specifphp artisan stub:publishied resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AssetStatus  $assetStatus
     * @return \Illuminate\Http\Response
     */
    public function update(AssetStatusRequest $request, AssetStatus $assetStatus)
    {
        $this->authorize('view', $assetStatus);
        $assetStatus->update($request->all());
        return ['message' => 'Data has been updated', 'data' => $assetStatus];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AssetStatus  $assetStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssetStatus $assetStatus)
    {
        $this->authorize('view', $assetStatus);
        $assetStatus->delete();
        return ['message' => 'Data has been deleted'];
    }
}
