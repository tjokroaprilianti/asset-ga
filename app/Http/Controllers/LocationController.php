<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Http\Requests\LocationRequest;
use App\Http\Resources\LocationCollection;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', Location::class);

        return Location::when($request->keyword, function ($q) use ($request) {
            $q->where(function ($q) use ($request) {
                $q->where('code', 'ILIKE', "%{$request->keyword}%")
                    ->orWhere('name', 'ILIKE', "%{$request->keyword}%")
                    ->orWhere('description', 'ILIKE', "%{$request->keyword}%");
            });
        })->whereNull('parent_id')->orderBy('code', 'asc')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LocationRequest $request)
    {
        $this->authorize('create', Location::class);
        $location = Location::create($request->all());
        return ['message' => 'Data has been saved', 'data' => $location];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        $this->authorize('view', $location);
        return $location;
    }

    /**
     * Update the specifphp artisan stub:publishied resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(LocationRequest $request, Location $location)
    {

        $this->authorize('update', $location);
        //dd($request->all());
        $location->update($request->all());
        return ['message' => 'Data has been updated', 'data' => $location];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        $this->authorize('delete', $location);
        $location->children()->delete();
        $location->delete();
        return ['message' => 'Data has been deleted'];
    }

    public function getList(Request $request)
    {
        return [
            'data' => Location::when($request->keyword, function ($q) use ($request) {
                $q->where('name', 'ILIKE', "%{$request->keyword}%");
            })->orderBy('name', 'asc')->get()
        ];
    }
}
