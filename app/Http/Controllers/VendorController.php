<?php

namespace App\Http\Controllers;

use App\Models\Vendor;
use App\Http\Requests\VendorRequest;
use App\Http\Resources\VendorCollection;
use Illuminate\Http\Request;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', Vendor::class);

        return new VendorCollection(
            Vendor::when($request->keyword, function ($q) use ($request) {
                $q->where(function ($q) use ($request) {
                    $q->where('name', 'ILIKE', "%{$request->keyword}%")
                        ->orWhere('address', 'ILIKE', "%{$request->keyword}%");
                });
            })->orderBy(
                $request->sort_field ?: 'name',
                $request->sort_direction == 'descending' ? 'desc' : 'asc'
            )->paginate($request->per_page)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VendorRequest $request)
    {
        $this->authorize('create', Vendor::class);
        $vendor = Vendor::create($request->all());
        return ['message' => 'Data has been saved', 'data' => $vendor];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function show(Vendor $vendor)
    {
        $this->authorize('view', $vendor);
        return $vendor;
    }

    /**
     * Update the specifphp artisan stub:publishied resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(VendorRequest $request, Vendor $vendor)
    {
        $this->authorize('update', $vendor);
        $vendor->update($request->all());
        return ['message' => 'Data has been updated', 'data' => $vendor];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vendor $vendor)
    {
        $this->authorize('delete', $vendor);
        $vendor->delete();
        return ['message' => 'Data has been deleted'];
    }

    public function getList(Request $request)
    {
        return [
            'data' => Vendor::when($request->keyword, function ($q) use ($request) {
                $q->where('name', 'ILIKE', "%{$request->keyword}%");
            })->orderBy('name', 'asc')->get()
        ];
    }
}
