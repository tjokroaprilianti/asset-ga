<?php

namespace App\Http\Controllers;

use App\Http\Requests\AssetCoaRequest;
use App\Http\Resources\AssetCoaCollection;
use App\Models\AssetCoa;
use Illuminate\Http\Request;

class AssetCoaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', AssetCoa::class);

        return new AssetCoaCollection(
            AssetCoa::when($request->keyword, function ($q) use ($request) {
                $q->where(function ($q) use ($request) {
                    $q->where('code', 'ILIKE', "%{$request->keyword}%")
                        ->orWhere('name', 'ILIKE', "%{$request->keyword}%");
                });
            })->orderBy(
                $request->sort_field ?: 'name',
                $request->sort_direction == 'descending' ? 'desc' : 'asc'
            )->paginate($request->per_page)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AssetCoaRequest $request)
    {
        $this->authorize('create', AssetCoa::class);
        $assetCoa = AssetCoa::create($request->all());
        return ['message' => 'Data has been saved', 'data' => $assetCoa];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AssetCoa  $assetCoa
     * @return \Illuminate\Http\Response
     */
    public function show(AssetCoa $assetCoa)
    {
        $this->authorize('view', $assetCoa);
        return $assetCoa;
    }

    /**
     * Update the specifphp artisan stub:publishied resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AssetCoa  $assetCoa
     * @return \Illuminate\Http\Response
     */
    public function update(AssetCoaRequest $request, AssetCoa $assetCoa)
    {
        $this->authorize('update', $assetCoa);
        $assetCoa->update($request->all());
        return ['message' => 'Data has been updated', 'data' => $assetCoa];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AssetCoa  $assetCoa
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssetCoa $assetCoa)
    {
        $this->authorize('delete', $assetCoa);
        $assetCoa->delete();
        return ['message' => 'Data has been deleted'];
    }
}
