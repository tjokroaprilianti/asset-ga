<?php

namespace App\Http\Controllers;

use App\Http\Requests\UnitRequest;
use App\Http\Resources\UnitCollection;
use App\Models\Unit;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', Unit::class);

        return Unit::with(['children'])->when($request->keyword, function ($q) use ($request) {
            $q->where('name', 'ILIKE', "%{$request->keyword}%");
        })->whereNull('parent_id')->orderBy('name', 'asc')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UnitRequest $request)
    {
        $this->authorize('create', Unit::class);
        $unit = Unit::create($request->all());
        return ['message' => 'Data has been saved', 'data' => $unit];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function show(Unit $unit)
    {
        $this->authorize('view', $unit);
        return $unit;
    }

    /**
     * Update the specifphp artisan stub:publishied resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function update(UnitRequest $request, Unit $unit)
    {
        $this->authorize('update', $unit);
        $unit->update($request->all());
        return ['message' => 'Data has been updated', 'data' => $unit];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Unit $unit)
    {
        $this->authorize('delete', $unit);
        $unit->children()->delete();
        $unit->delete();
        return ['message' => 'Data has been deleted'];
    }

    public function getList(Request $request)
    {
        return [
            'data' => Unit::when($request->keyword, function ($q) use ($request) {
                $q->where('name', 'ILIKE', "%{$request->keyword}%");
            })->orderBy('name', 'asc')->get()
        ];
    }
}
