<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SidebarMenuController extends Controller
{
    public function index(Request $request)
    {
        $menus = [
            [
                'label' => 'Dashboard',
                'icon' => 'el-icon-s-home',
                'path' => '/',
                'roles' => '*'
            ],
            [
                'label' => 'Asset',
                'icon' => 'el-icon-menu',
                'path' => '/asset',
                'roles' => '*'
            ],
            [
                'label' => 'Asset Movement',
                'icon' => 'el-icon-share',
                'path' => '/asset-movement',
                'roles' => [User::ROLE_ADMIN]
            ],
            [
                'label' => 'Master Data',
                'icon' => 'el-icon-coin',
                'path' => 'master-data',
                'roles' => [User::ROLE_ADMIN],
                'children' => [
                    [
                        'label' => 'Asset COA',
                        'icon' => 'el-icon-arrow-right',
                        'path' => '/asset-coa',
                        'roles' => [User::ROLE_ADMIN]
                    ],
                    [
                        'label' => 'Asset Category',
                        'icon' => 'el-icon-arrow-right',
                        'path' => '/asset-category',
                        'roles' => [User::ROLE_ADMIN]
                    ],
                    [
                        'label' => 'Asset Status',
                        'icon' => 'el-icon-arrow-right',
                        'path' => '/asset-status',
                        'roles' => [User::ROLE_ADMIN]
                    ],
                    // [
                    //     'label' => 'Cost Center',
                    //     'icon' => 'el-icon-arrow-right',
                    //     'path' => '/cost-center',
                    //     'roles' => [User::ROLE_ADMIN]
                    // ],
                    [
                        'label' => 'Locations',
                        'icon' => 'el-icon-arrow-right',
                        'path' => '/location',
                        'roles' => [User::ROLE_ADMIN]
                    ],
                    [
                        'label' => 'Unit',
                        'icon' => 'el-icon-arrow-right',
                        'path' => '/unit',
                        'roles' => [User::ROLE_ADMIN]
                    ],
                    [
                        'label' => 'User',
                        'icon' => 'el-icon-arrow-right',
                        'path' => '/user',
                        'roles' => [User::ROLE_ADMIN]
                    ],
                    [
                        'label' => 'Vendor',
                        'icon' => 'el-icon-arrow-right',
                        'path' => '/vendor',
                        'roles' => [User::ROLE_ADMIN]
                    ],
                ]
            ],

            [
                'label' => 'Mobile App',
                'icon' => 'el-icon-mobile-phone',
                'path' => '/mobile-app',
                'roles' => '*'
            ],
        ];

        $navigation = array_filter($menus, function ($item) {
            return is_array($item['roles']) ? in_array(Auth::user()->role, $item['roles']) : $item['roles'] == '*';
        });

        $navigation = array_map(function ($item) {
            if (isset($item['children'])) {
                $item['children'] = array_filter($item['children'], function ($i) {
                    return is_array($i['roles']) ? in_array(Auth::user()->role, $i['roles']) : $i['roles'] == '*';
                });
            }

            return $item;
        }, $navigation);

        return array_values($navigation);
    }
}
