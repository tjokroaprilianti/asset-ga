<?php

namespace App\Http\Controllers;

use App\Models\AssetCategory;
use App\Http\Requests\AssetCategoryRequest;
use App\Http\Resources\AssetCategoryCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AssetCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', AssetCategory::class);

        return new AssetCategoryCollection(
            AssetCategory::when($request->keyword, function ($q) use ($request) {
                $q->where(function ($q) use ($request) {
                    $q->where('name', 'ILIKE', "%{$request->keyword}%");
                });
            })->orderBy('id', 'asc')->paginate($request->per_page)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AssetCategoryRequest $request)
    {
        $this->authorize('create', AssetCategory::class);
        $assetCategory = AssetCategory::create($request->all());
        return ['message' => 'Data has been saved', 'data' => $assetCategory];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AssetCategory  $assetCategory
     * @return \Illuminate\Http\Response
     */
    public function show(AssetCategory $assetCategory)
    {
        $this->authorize('view', $assetCategory);
        return $assetCategory;
    }

    /**
     * Update the specifphp artisan stub:publishied resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AssetCategory  $assetCategory
     * @return \Illuminate\Http\Response
     */
    public function update(AssetCategoryRequest $request, AssetCategory $assetCategory)
    {
        $this->authorize('update', $assetCategory);
        $assetCategory->update($request->all());
        return ['message' => 'Data has been updated', 'data' => $assetCategory];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AssetCategory  $assetCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssetCategory $assetCategory)
    {
        $this->authorize('delete', $assetCategory);
        $assetCategory->children()->delete();
        $assetCategory->delete();
        return ['message' => 'Data has been deleted'];
    }

    public function import(Request $request)
    {
        $request->validate(['rows' => 'required']);

        $return = DB::transaction(function () use ($request) {

            foreach ($request->rows as $row) {
                if ($row['golongan'] == '' || $row['name'] == '') {
                    continue;
                }

                $data = [
                    'golongan' => str_pad($row['golongan'], 2, '0', STR_PAD_LEFT),
                    'bidang' => $row['bidang'] == '*' ? '*' : str_pad($row['bidang'], 2, '0', STR_PAD_LEFT),
                    'kelompok' => $row['kelompok'] == '*' ? '*' : str_pad($row['kelompok'], 2, '0', STR_PAD_LEFT),
                    'sub_kelompok' => $row['sub_kelompok'] == '*' ? '*' : str_pad($row['sub_kelompok'], 2, '0', STR_PAD_LEFT),
                    'sub_sub_kelompok' => $row['sub_sub_kelompok'] == '*' ? '*' : str_pad($row['sub_sub_kelompok'], 2, '0', STR_PAD_LEFT),
                    'name' => $row['name']
                ];

                $id = AssetCategory::create($data);
            }

            return response(['message' => 'Data has been imported successfully'], 201);
        });

        return $return;
    }
}
