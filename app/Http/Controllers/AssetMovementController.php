<?php

namespace App\Http\Controllers;

use App\Http\Requests\AssetMovementRequest;
use App\Http\Resources\AssetMovementCollection;
use App\Models\Asset;
use App\Models\AssetMovement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;


class AssetMovementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', AssetMovement::class);

        return new AssetMovementCollection(
            AssetMovement::with(['user'])->when($request->keyword, function ($q) use ($request) {
                $q->where('number', 'ILIKE', "%{$request->keyword}%");
            })->when($request->asset_id, function ($q) use ($request) {
                $q->whereHas('items', function ($q) use ($request) {
                    $q->where('asset_id', $request->asset_id);
                });
            })->orderBy(
                $request->sort_field ?: 'date',
                $request->sort_direction == 'descending' ? 'desc' : 'asc'
            )->paginate($request->per_page)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AssetMovementRequest $request)
    {
        $this->authorize('create', AssetMovement::class);

        $assetMovement = DB::transaction(function () use ($request) {
            $data = array_merge($request->all(), ['user_id' => Auth::id()]);
            $assetMovement = AssetMovement::create($data);
            $assetMovement->items()->createMany($request->items);
            $assetMovement->refresh();

            foreach ($assetMovement->items as $item) {
                $item->asset->update([
                    'location_id' => $item->location_id,
                    'status_id' => $item->status_id,
                ]);
            }

            return $assetMovement;
        });


        return ['message' => 'Data has been saved', 'data' => $assetMovement];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AssetMovement  $assetMovement
     * @return \Illuminate\Http\Response
     */
    public function show(AssetMovement $assetMovement)
    {
        $this->authorize('view', $assetMovement);
        return $assetMovement->load(['items.asset', 'items.status', 'items.location']);
    }

    /**
     * Update the specifphp artisan stub:publishied resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AssetMovement  $assetMovement
     * @return \Illuminate\Http\Response
     */
    public function update(AssetMovementRequest $request, AssetMovement $assetMovement)
    {
        $this->authorize('update', $assetMovement);

        DB::transaction(function () use ($request, $assetMovement) {
            $assetMovement->update($request->all());
            $assetMovement->items()->delete();
            $assetMovement->items()->createMany($request->items);
            $assetMovement->refresh();

            foreach ($assetMovement->items as $item) {
                $item->asset->update([
                    'location_id' => $item->location_id,
                    'status_id' => $item->status_id,
                ]);
            }
        });

        return ['message' => 'Data has been updated', 'data' => $assetMovement];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AssetMovement  $assetMovement
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssetMovement $assetMovement)
    {
        $this->authorize('delete', $assetMovement);

        DB::transaction(function () use ($assetMovement) {
            $assetMovement->delete();
            $assetMovement->items()->delete();
        });

        return ['message' => 'Data has been deleted'];
    }

    public function printBeritaAcara(AssetMovement $assetMovement)
    {
        //$haritanggal =
        //dd($assetMovement);
        $pdf = PDF::loadView('asset-movement.berita-acara', compact('assetMovement'));
        return $pdf->stream();
    }
}
