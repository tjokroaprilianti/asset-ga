<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProvinsiRequest;
use App\Http\Resources\ProvinsiCollection;
use App\Models\Provinsi;
use Illuminate\Http\Request;

class ProvinsiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', Provinsi::class);

        return new ProvinsiCollection(
            Provinsi::when($request->keyword, function($q) use ($request) {
                $q->where(function($q) use ($request) {
                    $q->where('field1', 'ILIKE', "%{$request->keyword}%")
                        ->orWhere('field2', 'ILIKE', "%{$request->keyword}%");
                });
            }) ->orderBy(
                $request->sort_field ?: 'name',
                $request->sort_direction == 'descending' ? 'desc' : 'asc'
            )->paginate($request->per_page)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProvinsiRequest $request)
    {
        $this->authorize('create', Provinsi::class);
        $provinsi = Provinsi::create($request->all());
        return ['message' => 'Data has been saved', 'data' => $provinsi];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Provinsi  $provinsi
     * @return \Illuminate\Http\Response
     */
    public function show(Provinsi $provinsi)
    {
        $this->authorize('view', $provinsi);
        return $provinsi;
    }

    /**
     * Update the specifphp artisan stub:publishied resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Provinsi  $provinsi
     * @return \Illuminate\Http\Response
     */
    public function update(ProvinsiRequest $request, Provinsi $provinsi)
    {
        $this->authorize('update', $provinsi);
        $provinsi->update($request->all());
        return ['message' => 'Data has been updated', 'data' => $provinsi];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Provinsi  $provinsi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Provinsi $provinsi)
    {
        $this->authorize('delete', $provinsi);
        $provinsi->delete();
        return ['message' => 'Data has been deleted'];
    }
    public function getList(Request $request)
    {
        return [
            'data' => Provinsi::when($request->keyword, function ($q) use ($request) {
                $q->where('city', 'ILIKE', "%{$request->keyword}%");
            })->orderBy('city', 'asc')->get()
        ];
    }
}
