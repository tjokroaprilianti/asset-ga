<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AssetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'asset_category_id' => 'required',
            'unit_id' => 'required',
            'location_id' => 'required',
            'model' => 'required',
            'serial_number' => 'required',
            'year' => 'required'
        ];
    }
}
