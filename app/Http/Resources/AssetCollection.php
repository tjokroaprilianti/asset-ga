<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AssetCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function ($item) {
                return array_merge($item->toArray(), [
                    'location' => $item->location ? $item->location->name : '',
                    'status' => $item->status ? $item->status->code : '',
                    'category' => $item->assetCategory ? $item->assetCategory->name : '',
                    'vendor' => $item->vendor ? $item->vendor->name : '',
                    'unit' => $item->unit ? $item->unit->name : '',
                    'location' => $item->location ? $item->location->name : '',
                    'unit' => $item->unit ? $item->unit->name : '',
                ]);
            })
        ];
    }
}
